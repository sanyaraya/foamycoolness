//
//  Constants.swift
//  FoamyCoolness
//
//  Created by Александр on 22.05.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import Foundation

// MARK: - API Constants

enum API {
    static let BaseURL = "https://sandbox-api.brewerydb.com/v2/"
    static let APIKey  = "1aefb418f81eec379f65d9bbf7560466"
}

// MARK: - Images

enum ImagesNames {
    static let defaultBeer = "app_icon_default_beer.png"
    static let defaultBrewery = "app_icon_default_brewery.png"
    static let starSelected = "app_selected_start.png"
    static let starNotSelected = "app_start_not_selected.png"
    static let bubble1 = "Bubble1.png"
    static let bubble2 = "Bubble2.png"
    static let bubble  = "Bubble"
}

// MARK: - UserDefaults

enum UserDefaultsKeys {
    static let lastBeerSearch = "lastBeerSearch"
    static let lastTabVisited = "lastTabVisited"
}

enum Times {
    static let splashTime = 3.0
}

// MARK: - Strings

enum ErrorTitles {
    static let Title              = "Ooooopss"
    static let Default            = "Something went wrong"
    static let EnableUserLocation = "We need your location. Please, enable finding user locations"
}

enum StringsConstants {
    static let beerDescriptionAbsent = "No Beer description :("
    static let breweryDescriptionAbsent = "No Brewery description :("
}

enum Symbols {
    static let slash = "/"
}

enum DocumentPaths {
    static let favouriteBeers = "Beers.plist"
}

// MARK: - Storyboards

enum Storyboards {
    static let Splash = "Splash"
    static let BeerDetails = "BeerDetails"
    static let BreweryDetails = "BreweryDetails"
}

enum TabPages: Int {
    case beersSearch
    case breweriesNearby
    case favouriteBeers
    case pagesNumber
}

// MARK: - typealias

typealias Coordinate = (latitude: Double, longtitude: Double)
