//
//  Colors.swift
//  FoamyCoolness
//
//  Created by Александр on 07.06.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import Foundation

enum ColorNames: String {
    case labelOrange
    case labelGreen
}

enum Colors {
    static let labelOrange = UIColor(named: ColorNames.labelOrange.rawValue)
    static let labelGreen = UIColor(named: ColorNames.labelGreen.rawValue)
}
