//
//  AppDelegate.swift
//  FoamyCoolness
//
//  Created by Александр on 22.05.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        self.loadUserDefaults()
        self.pushSplashScreen()
        FavouriteManager.shared.loadFavouriteBeers()

        DispatchQueue.main.asyncAfter(deadline: .now() + Times.splashTime) {
            self.popSplashScreen()
        }

        return true
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        FavouriteManager.shared.saveFavouriteBeers()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        FavouriteManager.shared.saveFavouriteBeers()
    }

    // MARK: - Additional Methods

    func loadUserDefaults() {
        self.getTabbarController()?.selectedIndex = UserDefaults.standard.integer(forKey: UserDefaultsKeys.lastTabVisited)
    }

    func getCurrentNav() -> UINavigationController? {
        guard let tabBar = self.getTabbarController() else {
            return nil
        }
        return tabBar.viewControllers?[tabBar.selectedIndex] as? UINavigationController
    }
    
    func getTabbarController() -> UITabBarController? {
        return self.window?.rootViewController as? UITabBarController
    }

    // MARK: - Push/Pop screen

    func pushSplashScreen() {
        guard let navViewController = getCurrentNav() else {
            return
        }
        if let topVC = navViewController.topViewController {
            navViewController.setViewControllers([topVC, SplashViewController.storyboardInstance()!], animated: false)
        }
    }

    func popSplashScreen() {
        self.getCurrentNav()?.popViewController(animated: true)
    }
}
