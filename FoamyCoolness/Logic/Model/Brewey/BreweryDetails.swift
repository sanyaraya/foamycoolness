//
//  Brewery.swift
//  FoamyCoolness
//
//  Created by Александр on 23.05.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import Foundation

struct BreweryDetails: Decodable {

    // MARK: - Properties

    let id: String
    let name: String
    let website: String?
    let breweryDescription: String?
    let imageURL: URL?

    var description: String {
        return self.breweryDescription ?? StringsConstants.breweryDescriptionAbsent
    }

    // MARK: - CodingKeys

    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case website
        case images
        case breweryDescription = "description"
    }

    // MARK: - ImagesKeys

    private enum ImagesKeys {
        static let large = "squareLarge"
    }

    // MARK: - Initializing

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try container.decode(String.self, forKey: .id)
        self.breweryDescription = try? container.decode(String.self, forKey: .breweryDescription)
        self.name = try container.decode(String.self, forKey: .name)
        self.website = try? container.decode(String.self, forKey: .website)

        let photosURL = try? container.decode([String: String].self, forKey: .images)

        if let image = photosURL?[ImagesKeys.large] {
            self.imageURL = URL(string: image)
        } else {
            self.imageURL = nil
        }
    }
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
        self.website = nil
        self.imageURL = nil
        self.breweryDescription = nil
    }
    
    static func emptyDetails() -> BreweryDetails {
        let details = BreweryDetails(id: "default", name: "No details")
        return details
    }
}
