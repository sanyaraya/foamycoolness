//
//  BreweryHead.swift
//  FoamyCoolness
//
//  Created by Александр on 25.05.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import Foundation

struct Brewery: Decodable {

    // MARK: - Properties

    let id: String
    let name: String
    let phone: String?
    let website: String?
    let streetAddress: String?
    let brewery: BreweryDetails
    let distance: BreweryDistance?

    // MARK: - CodingKeys

    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case phone
        case brewery
        case website
        case distance
        case streetAddress
    }

    // MARK: - Decodable

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.name = try container.decode(String.self, forKey: .name)
        self.phone = try? container.decode(String.self, forKey: .phone)
        self.website = try? container.decode(String.self, forKey: .website)
        self.streetAddress = try? container.decode(String.self, forKey: .streetAddress)
        self.brewery = try container.decode(BreweryDetails.self, forKey: .brewery)

        if let distance = try? container.decode(Double.self, forKey: .distance) {
            self.distance = BreweryDistance(distance: distance)
        } else {
            self.distance = nil
        }
    }
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
        
        self.brewery = BreweryDetails.emptyDetails()
        self.phone = nil
        self.website = nil
        self.distance = nil
        self.streetAddress = nil
    }
    
    static func emptyBrewery() -> Brewery {
        return Brewery(id: "Empty", name: "Empty Brewery")
    }
}
