//
//  BreweryDistance.swift
//  FoamyCoolness
//
//  Created by Александр on 03.06.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import Foundation

struct BreweryDistance {

    // MARK: - Constants
    enum DistanceEstimate {
        static let Near = 3.0
        static let NotSoNear = 6.0
    }

    enum DistanceStates {
        case unknown
        case nearby
        case normal
        case far
    }

    // MARK: - Properties

    var distanceEstimate: DistanceStates = .unknown
    let distance: Double

    // MARK: - Decodable

    init(distance: Double) {
        self.distance = distance

        switch distance {
        case ...DistanceEstimate.Near:
            self.distanceEstimate = .nearby
        case ..<DistanceEstimate.NotSoNear:
            self.distanceEstimate = .normal
        default:
            self.distanceEstimate = .far
        }
    }
}
