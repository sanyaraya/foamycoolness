//
//  Abv
//  FoamyCoolness
//
//  Created by Александр on 29.05.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import Foundation

struct Abv {
    // MARK: - Constant
    enum BeerAbvEdges {
        static let Low  = 5.0
        static let Middle = 8.0
    }

    enum AbvStates {
        case unknown
        case low
        case middle
        case high
    }

    // MARK: - Properties

    var abvLevel: AbvStates = .unknown
    let level: String
    
    init(level: String) {
        self.level = level
        switch Double(level) {
            case nil:
            self.abvLevel = .unknown
            case (...BeerAbvEdges.Low)?:
            self.abvLevel = .low
            case (..<BeerAbvEdges.Middle)?:
            self.abvLevel = .middle
            default:
            self.abvLevel = .high
        }
    }
}
