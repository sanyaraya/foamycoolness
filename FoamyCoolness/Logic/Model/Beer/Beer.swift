//
//  Beer.swift
//  FoamyCoolness
//
//  Created by Александр on 22.05.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import Foundation

struct Beer: Codable, Equatable, Hashable {

    // MARK: - Properties

    let id: String
    let name: String
    let title: String
    let beerDescription: String?

    let abv: Abv?

    let photosURL: [String: String]?
    let iconURL: URL?
    let mediumURL: URL?

    var description: String {
        return self.beerDescription ?? StringsConstants.beerDescriptionAbsent
    }

    // MARK: - CodingKeys

    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case abv
        case title = "nameDisplay"
        case photosURL = "labels"
        case beerDescription = "description"
    }

    // MARK: - ImagesPaths

    private enum ImagesKeys {
        static let icon = "icon"
        static let medium = "medium"
    }

    // MARK: - Codable

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.id, forKey: .id)
        try container.encode(self.name, forKey: .name)
        try container.encode(self.title, forKey: .title)
        try container.encode(self.abv?.level, forKey: .abv)
        try container.encode(self.photosURL, forKey: .photosURL)
        try container.encode(self.beerDescription, forKey: .beerDescription)
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try container.decode(String.self, forKey: .id)
        self.name = try container.decode(String.self, forKey: .name)
        self.title = try container.decode(String.self, forKey: .title)
        self.photosURL = try? container.decode([String: String].self, forKey: .photosURL)
        self.beerDescription = try? container.decode(String.self, forKey: .beerDescription)
        
        if let icon = self.photosURL?[ImagesKeys.icon] {
            self.iconURL = URL(string: icon)
        } else {
            self.iconURL = nil
        }
        
        if let medium = self.photosURL?[ImagesKeys.medium] {
            self.mediumURL = URL(string: medium)
        } else {
            self.mediumURL = nil
        }
        
        if let abv = try? container.decode(String.self, forKey: .abv) {
            self.abv = Abv(level: abv)
        } else {
            self.abv = nil
        }
    }
    
    init(id: String, name: String, title: String) {
        self.id = id
        self.name = name
        self.title = title
        
        self.abv = nil
        self.iconURL = nil
        self.mediumURL = nil
        self.photosURL = nil
        self.beerDescription = nil
    }
    
    // MARK: - Hashable

    func hash(into hasher: inout Hasher) {
        hasher.combine(self.id)
        hasher.combine(self.name)
        hasher.combine(self.title)
    }

    // MARK: - Equatable

    public static func == (lhs: Beer, rhs: Beer) -> Bool {
        return lhs.id == rhs.id
    }
    
    // MARK: - EmptyBeer

    static func emptyBeer() -> Beer {
        return Beer(id: "default", name: "Empty beer", title: "Empty beer")
    }
}
