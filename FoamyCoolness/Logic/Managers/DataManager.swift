//
//  DataManager.swift
//  FoamyCoolness
//
//  Created by Александр on 22.05.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit
import Alamofire
import RxSwift
import RxAlamofire
import MoreCodable

typealias RequestHandler = ((Data?, Error?) -> Void)

class DataManager: NSObject {

    // MARK: - Constants

    private enum Packages {
        static let Search = "search"
        static let Beer = "beer"
        static let Beers = "beers"
        static let Brewery = "brewery"
        static let Breweries = "breweries"
        static let GeoSearch = "search/geo/point"
    }

    private enum Defaults {
        static let DefaultRadius = 15
    }

    // MARK: - Properties

    static let shared = DataManager()

    private var rxAfManager = SessionManager.default
    
    private var afManager: Alamofire.SessionManager {
       return Alamofire.SessionManager(configuration: URLSessionConfiguration.default)
    }

    // MARK: - Beers
    
    func getAllBeers(page: Int) -> Observable<[Beer]> {
        let params = ParamsBuilder().forPage(page: page).build()
        let response = self.executeAndGetJSON(request: API.BaseURL + Packages.Beers, params: params, method: .get)
        return parseBeersReposnse(response: response)
    }

    private func getBeers(params: [String: Any]?) -> Observable<[String : Any]> {
        return self.executeAndGetJSON(request: API.BaseURL + Packages.Search, params: params ?? [:], method: .get)
    }
    
    func getBeers(name: String = "", page: Int) -> Observable<[Beer]> {
        let params = ParamsBuilder().forPage(page: page).search(text: name).searchBeer().build()
        return parseBeersReposnse(response: getBeers(params: params))
    }

    
  func getBeersByBreweryId(brewId: String) -> Observable<[Beer]> {
        let params = ParamsBuilder().build()
        return self.executeAndGetJSON(request: API.BaseURL + Packages.Brewery + Symbols.slash + brewId + Symbols.slash + Packages.Beers, params: params, method: .get).map {
                return $0["data"] as? [[String : Any]] ?? []
            }.map { results in
                return results.map {
                    let beer = try? DictionaryDecoder().decode(Beer.self, from: $0)
                    return  beer ?? Beer.emptyBeer()
                }
            }
    }
    
    func getBeerById(beerId: String) -> Observable<Beer> {
        let params = ParamsBuilder().build()
        return self.executeAndGetJSON(request: API.BaseURL + Packages.Beer + Symbols.slash + beerId, params: params, method: .get).map {beer in
            return beer["data"] as? [String : Any] ?? [:]
            }.map {
                let beer = try? DictionaryDecoder().decode(Beer.self, from: $0)
                return  beer ?? Beer.emptyBeer()
        }
    }
    
    // MARK: - Breweries

    func getBreweriesNearby(coord: Coordinate, page: Int) -> Observable<[Brewery]> {
        let params = ParamsBuilder().searchLocation(center: coord).searchCircle(radius: Defaults.DefaultRadius).forPage(page: page).build(
        )
        return self.executeAndGetJSON(request: API.BaseURL + Packages.GeoSearch, params: params, method: .get).map{ brew in return brew["data"] as? [[String : Any]] ?? []}.map {results in
            return results.map{
                let brewery = try? DictionaryDecoder().decode(Brewery.self, from: $0)
                return brewery ?? Brewery.emptyBrewery()
            }
        }
    }
    
    //MARK: - Helper
    
    func parseBeersReposnse(response: Observable<[String : Any]>) -> Observable<[Beer]> {
        return response.map {data in return (data["data"] as? [[String: Any]]) ?? []}.map { beers in
            return beers.map{
                let beer = try? DictionaryDecoder().decode(Beer.self, from: $0)
                return  beer ?? Beer.emptyBeer()
            }
        }
    }
    
    //MARK: - Request
    
    func executeAndGetJSON(request: String, params: [String: Any], method: HTTPMethod) -> Observable<[String: Any]> {
        return self.rxAfManager.rx.json(method, request, parameters: params).map { data in
            return (data as? [String : Any]) ?? [:]
        }
    }
}

