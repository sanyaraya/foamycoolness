//
//  BaseDataHolderTableManager.swift
//  FoamyCoolness
//
//  Created by Александр on 23.05.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

// MARK: - DataHolderTableManager

class DataHolderTableManager: NSObject {

    // MARK: - Constants

    private enum Defaults {
        static let pullEdge: CGFloat = 100.0
        static let footerHeight: CGFloat = 55.0
        static let downPosition: CGFloat = 0.0
    }

    // MARK: - Properties

    var tvContent: UITableView?

    var footerView: LoadableFooterView?

    var loadOnScrolledDown = true
    
    let disposeBag = DisposeBag()

    let loadMoreItems = PublishSubject<Void>()

    init(table: UITableView, loadOnScrolledDown: Bool = true) {
        self.tvContent = table
        super.init()
        self.tvContent?.delegate = self
        self.loadOnScrolledDown = loadOnScrolledDown
       
        self.bindScroll()
    }
    
    func bindScroll() {
        self.tvContent?.rx.didEndDecelerating
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { _ in
                
                guard let footer = self.footerView else {
                    return
                }
                
                guard self.loadOnScrolledDown, let tv = self.tvContent else {
                    return
                }
                
                let pullHeight = abs(tv.calculatePullHeight())
                
                guard pullHeight == Defaults.downPosition else {
                    return
                }
                footer.isHidden = false
                self.loadMoreItems.onNext(())
            }).disposed(by: self.disposeBag)
    }
}


// MARK: - UITableViewDelegate

extension DataHolderTableManager: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if self.footerView == nil {
            self.footerView = LoadableFooterView(frame: CGRect(x: 0, y: 0, width: self.tvContent!.frame.width, height: Defaults.footerHeight))
        }

        return self.loadOnScrolledDown ? self.footerView : nil
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return self.loadOnScrolledDown ? Defaults.footerHeight : 0.0
    }

    func tableView(_ tableView: UITableView, didEndDisplayingFooterView view: UIView, forSection section: Int) {
        DispatchQueue.main.async {
            self.footerView?.isHidden = true
        }
    }
}
