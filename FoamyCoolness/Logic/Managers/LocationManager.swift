//
//  LocationManager.swift
//  Flickr
//
//  Created by Александр on 23.04.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import Foundation
import CoreLocation

// MARK: - LocationManagerDelegate

@objc protocol LocationManagerDelegate: class {

    @objc optional func showError(description: String)

    @objc optional func locationDidUpdated(location: CLLocation)
}

// MARK: - LocationManager

class LocationManager: NSObject {

    // MARK: - Constants

    private enum Defaults {
        static let metersDeltaToReload = 10.0
    }

    // MARK: - Properties

    let locationManager  = CLLocationManager()
    var updatingLocation = false
    var currentLocation: CLLocation?

    weak var delegate: LocationManagerDelegate?

    var location: Coordinate? {
        guard let loc = self.currentLocation else {
            return nil
        }
        return Coordinate(loc.coordinate.latitude, loc.coordinate.longitude)
    }

    // MARK: - Methods

    func searchLocation() {
        let authStatus = CLLocationManager.authorizationStatus()

        if authStatus == .notDetermined {
            locationManager.requestWhenInUseAuthorization()
        }

        if authStatus == .denied || authStatus == .restricted {
            self.delegate?.showError?(description: ErrorTitles.EnableUserLocation)
            return
        }
        if self.updatingLocation {
            self.stopLocationManager()
        } else {
            self.currentLocation = nil
            startLocationManager()
        }
    }

    func startLocationManager() {
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            self.locationManager.startUpdatingLocation()
            self.updatingLocation = true
        }
    }

    func stopLocationManager() {
        if self.updatingLocation {
            self.locationManager.stopUpdatingLocation()
            self.locationManager.delegate = nil
            self.updatingLocation = false
        }
    }
}

// MARK: - CLLocationManagerDelegate

extension LocationManager: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager,
                         didFailWithError error: Error) {
        self.delegate?.showError?(description: error.localizedDescription)
    }

    func locationManager(_ manager: CLLocationManager,
                         didUpdateLocations locations: [CLLocation]) {
        guard let newLocation = locations.last else {
            return
        }

        guard let currentLoc = self.currentLocation else {
            self.currentLocation = newLocation
           self.delegate?.locationDidUpdated?(location: newLocation)
            return
        }

        if newLocation != self.currentLocation {
            let distanceInMeters = currentLoc.distance(from: newLocation)

            if distanceInMeters > Defaults.metersDeltaToReload {
                self.currentLocation = newLocation
                self.delegate?.locationDidUpdated?(location: newLocation)
            }
        }
    }
}

