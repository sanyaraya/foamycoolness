//
//  ImageLoader.swift
//  Flickr
//
//  Created by Александр on 27.04.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ImageLoader: NSObject {
    
    // MARK: - loading
    
    func loadImageUrl(url: URL) -> Observable<UIImage?> {
        return URLSession.shared.rx.data(request: URLRequest(url: url))
            .map { data in
                return UIImage(data: data)
        }.catchErrorJustReturn(nil)
    }
    
    func loadIcon(model: Beer) -> Observable<UIImage?> {
        if let icon = model.iconURL {
            return self.loadImageUrl(url: icon)
        } else if let medium = model.mediumURL {
            return self.loadImageUrl(url: medium)
        } else {
            return Observable.just(nil)
        }
    }
    
    func loadMediumImage(model: Beer) -> Observable<UIImage?> {
        if let medium = model.mediumURL {
            return self.loadImageUrl(url: medium)
        } else {
            return Observable.just(nil)
        }
    }
}

