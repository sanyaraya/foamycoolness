//
//  FavouriteManager.swift
//  FoamyCoolness
//
//  Created by Александр on 27.05.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class FavouriteManager: NSObject {

    // MARK: - Properties

    static let shared = FavouriteManager()

    var favouriteBeers: BehaviorRelay<[Beer]> = BehaviorRelay(value: [])

    subscript(index: Int) -> Beer {
        let beers = self.favouriteBeers.value
        return beers[index]
    }

    // MARK: - Beer Actions

    func addBeer(beer: Beer) {
        if self.favouriteBeers.value.filter({beer.id == $0.id}).count == 0 {
            let newValue = self.favouriteBeers.value + [beer]
            self.favouriteBeers.accept(newValue)
        }
    }

    func removeBeer(beer: Beer) {
        let newValue = self.favouriteBeers.value.filter{ $0.id != beer.id}
        self.favouriteBeers.accept(newValue)
    }

    func isFavourite(beerId: String) -> Bool {
        return self.favouriteBeers.value.filter({$0.id == beerId}).count != 0
    }

    // MARK: - Saving/Loading

    func saveFavouriteBeers() {
        let encoder = PropertyListEncoder()

        if let data = try? encoder.encode(self.favouriteBeers.value) {
            try? data.write(to: dataFilePath(),
                            options: Data.WritingOptions.atomic)
        }
    }

    func loadFavouriteBeers() {
        let path = dataFilePath()

        guard let data = try? Data(contentsOf: path) else {
            return
        }

        let decoder = PropertyListDecoder()

        if let beers = try? decoder.decode([Beer].self, from: data) {
            self.favouriteBeers.accept(beers)
        }
    }

    // MARK: - Helper

    func dataFilePath() -> URL {
        return documentsDirectory().appendingPathComponent(
            DocumentPaths.favouriteBeers)
    }
}
