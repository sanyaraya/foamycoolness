//
//  RxLocationManagerProxy.swift
//  FoamyCoolness
//
//  Created by Александр on 7/10/19.
//  Copyright © 2019 Александр. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import CoreLocation

extension LocationManager: HasDelegate {
    public typealias Delegate = LocationManagerDelegate
}

class RxLocationManagerProxy: DelegateProxy<LocationManager, LocationManagerDelegate>, DelegateProxyType, LocationManagerDelegate {
   
    public weak private(set) var locationManager: LocationManager?
    
    init(locationManager: LocationManager) {
        self.locationManager = locationManager
        super.init(parentObject: locationManager, delegateProxy: RxLocationManagerProxy.self)
    }
    
    static func registerKnownImplementations() {
        self.register{RxLocationManagerProxy.init(locationManager: $0)}
    }
}

extension Reactive where Base: LocationManager {
    var delegate: DelegateProxy<LocationManager, LocationManagerDelegate> {
        return RxLocationManagerProxy.proxy(for: base)
    }
    
    var didUpdateLocations: Observable<CLLocation> {
        return delegate.methodInvoked(#selector(LocationManagerDelegate.locationDidUpdated(location:))).map {
            $0[0] as? CLLocation ?? self.base.currentLocation ?? CLLocation(latitude: 0, longitude: 0)
        }
    }
    
    var didGetErrorToDisplay: Observable<String> {
        return delegate.methodInvoked(#selector(LocationManagerDelegate.showError(description:))).map {
            $0[0] as? String ?? "Unknown Error"
        }
    }
}

