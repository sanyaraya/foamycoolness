//
//  DataHolder.swift
//  FoamyCoolness
//
//  Created by Александр on 22.05.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

// MARK: - Instance

class DataHolder<T>: NSObject {

    // MARK: - Properites

    var currentPage: Int = 0
    var items: BehaviorRelay<[T]> = BehaviorRelay(value: [])
    
    // MARK: - Getters

    var itemsNumber: Int {
        return items.value.count
    }

    subscript(index: Int) -> T? {
        guard index >= 0 && index < items.value.count else {
            return nil
        }
        return items.value[index]
    }

    // MARK: - Helpers

    func removeAll() {
        self.currentPage = 0
        self.items.accept([])
    }

    func appendItems(items: [T]) {
        let newSet = self.items.value + items
        self.items.accept(newSet)
        self.currentPage += 1
    }
}

// MARK: - BeerHolder

extension DataHolder where T == Beer {
    func reload(search: String) -> Observable<Bool> {
        self.removeAll()
        return loadMore(search: search)
    }
    
    func loadMore(search: String) -> Observable<Bool> {
        if search.isEmpty {
            return DataManager.shared.getAllBeers(page: self.currentPage + 1).map {
                self.appendItems(items: $0)
                return true
                }
        } else {
            return DataManager.shared.getBeers(name: search, page: self.currentPage + 1).map {
                self.appendItems(items: $0)
                return true
            }
        }
    }
}

// MARK: - Brewery

extension DataHolder where T == Brewery {
    func reload(coord: Coordinate) -> Observable<Bool> {
        self.removeAll()
        return self.loadMore(coord: coord)
    }
    
    func loadMore(coord: Coordinate) -> Observable<Bool> {
        return DataManager.shared.getBreweriesNearby(coord: coord, page: self.currentPage + 1).map {
            self.appendItems(items: $0)
            return true
        }
    }
}
