//
//  FavouriteIndicator.swift
//  FoamyCoolness
//
//  Created by Александр on 25.05.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit
import Then
import RxSwift
import RxCocoa
import RxGesture

// MARK: - FavouriteIndicator

class FavouriteIndicator: BaseView {
    
    //MARK: - Constants
    
    private enum Defaults {
        static let titleTrailing: CGFloat = -10
        static let imageTrailing: CGFloat = -5
        static let imageHeight: CGFloat = 2 / 3.0
    }
    
    
    // MARK: - Properties
    
    let disposeBag = DisposeBag()
    
    let favouriteToggled = PublishSubject<Bool>()
    
    let isFavourite = BehaviorRelay<Bool>(value: false)

    //MARK: - Components

    let lblTitle = UILabel().then {
        $0.translatesAutoresizingMaskIntoConstraints = false
        $0.textAlignment = .natural
        $0.textColor = .lightGray
        $0.text = "Add to fav"
    }
    
    let contentView = UIView()
    
    let ivStar = UIImageView().then {
        $0.image = UIImage(named: ImagesNames.starNotSelected)
        $0.translatesAutoresizingMaskIntoConstraints = false
        $0.widthAnchor.constraint(equalTo: $0.heightAnchor)
        $0.contentMode = .scaleAspectFit
    }
    
    // MARK: - Initializing

    override func commonInit() {
        self.contentView.frame = self.bounds
        self.addSubview(self.contentView)
        self.contentView.addSubview(self.lblTitle)
        self.contentView.addSubview(self.ivStar)
        
        self.addViewsConstraints()
        self.bindUI()
    }
    
    func bindUI() {
        self.rx.tapGesture()
            .when(.recognized)
            .subscribe(onNext: {[unowned self] _ in
                self.isFavourite.accept(!self.isFavourite.value)
                self.favouriteToggled.onNext(self.isFavourite.value)
            }).disposed(by: disposeBag)
        
        self.isFavourite.subscribe(onNext: { (fav) in
            if fav {
                self.setFavourite()
            } else {
                self.setUnfavourite()
            }
        }).disposed(by: self.disposeBag)
    }

    func setFavourite() {
        self.ivStar.image = UIImage(named: ImagesNames.starSelected)
        self.lblTitle.textColor = Colors.labelOrange
        self.lblTitle.text = "Favourite "
    }
    
    func setUnfavourite() {
        self.ivStar.image = UIImage(named: ImagesNames.starNotSelected)
        self.lblTitle.textColor = .lightGray
        self.lblTitle.text = "Add to fav"
    }
    
    private func addViewsConstraints() {
        self.lblTitle.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor).isActive = true
        self.lblTitle.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: Defaults.titleTrailing).isActive = true
        
        self.ivStar.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor).isActive = true
        self.ivStar.trailingAnchor.constraint(equalTo: self.lblTitle.leadingAnchor, constant: Defaults.imageTrailing).isActive = true
        self.ivStar.heightAnchor.constraint(equalTo: self.contentView.heightAnchor, multiplier: Defaults.imageHeight, constant: 0).isActive = true
    }
}
