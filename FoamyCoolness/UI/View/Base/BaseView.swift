//
//  BaseView.swift
//  Together
//
//  Created by dima on 04.09.2018.
//  Copyright © 2018 com.Think.Dev.Together. All rights reserved.
//

import UIKit

class BaseView: UIView, NibLoadable {
    static var nibName: String?

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }

    // MARK: - CommonInitable

    func commonInit() {}

}
