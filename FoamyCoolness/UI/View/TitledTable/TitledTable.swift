//
//  TitledTable.swift
//  FoamyCoolness
//
//  Created by Александр on 25.05.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit

class TitledTable: UITableView {

    // MARK: - Properties

    lazy var noItemsView = NoItemsView(frame: self.bounds)

    var noItemsViewIsHidden: Bool = true {
        didSet {
            if noItemsViewIsHidden {
                self.hideNoItemsView()
            } else {
                self.showNoItemsView()
            }
        }
    }

    // MARK: - Actions

    func showNoItemsView() {
        self.addSubview(self.noItemsView)
    }

    func hideNoItemsView() {
        self.noItemsView.removeFromSuperview()
    }
}
