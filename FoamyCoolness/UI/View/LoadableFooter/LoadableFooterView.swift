//
//  LoadableFooterView.swift
//  FoamyCoolness
//
//  Created by Александр on 23.05.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit

class LoadableFooterView: BaseView {

    @IBOutlet var contentView: UIView!

    override func commonInit() {
        self.loadNib()
        self.contentView.frame = self.bounds
        self.addSubview(self.contentView)
    }
}
