//
//  NoItems.swift
//  FoamyCoolness
//
//  Created by Александр on 25.05.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit

class NoItemsView: BaseView {
    
    // MARK: - Properties
    
    let lblTitle = UILabel().then {
        $0.translatesAutoresizingMaskIntoConstraints = false
        $0.textAlignment = .center
        $0.textColor = .white
        $0.text = "No Items"
    }
    
    let contentView = UIView().then {
        $0.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    }
    
    //MARK: - Initializing
    
    override func commonInit() {
        self.contentView.frame = self.bounds
        self.addSubview(self.contentView)
        self.contentView.addSubview(self.lblTitle)
        
        self.lblTitle.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor).isActive = true
        self.lblTitle.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor).isActive = true
    }
}
