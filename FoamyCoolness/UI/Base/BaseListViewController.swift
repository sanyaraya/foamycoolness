//
//  BaseListViewController.swift
//  FoamyCoolness
//
//  Created by Александр on 23.05.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit

class BaseListViewController: UIViewController {

    // MARK: - Properties

    @IBOutlet weak var tvContent: TitledTable?

    var tableManager: DataHolderTableManager?

    var spinner: UIActivityIndicatorView?

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.addSpinner()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let presenter = self as? TabPresentable {
            UserDefaults.standard.set(presenter.tabPage.rawValue, forKey: UserDefaultsKeys.lastTabVisited)
        }
    }

    // MARK: - Spinner

    func addSpinner() {
        self.spinner = UIActivityIndicatorView(style: .whiteLarge)
        self.spinner?.hidesWhenStopped = true
        self.spinner?.color = .darkText
        self.spinner?.center = self.view.center
        self.view.addSubview(self.spinner!)
    }

    func startSpinner() {
        self.spinner?.startAnimating()
    }

    func stopSpinner() {
        self.spinner?.stopAnimating()
    }
}
