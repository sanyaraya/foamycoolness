//
//  FavouriteBeersViewController.swift
//  FoamyCoolness
//
//  Created by Александр on 27.05.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class FavouriteBeersViewController: BaseListViewController {

    // MARK: - Properties

    let data = FavouriteManager.shared
    let disposeBag = DisposeBag()

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCells()
        
        self.bindTable()

        self.tableManager = DataHolderTableManager(table: self.tvContent!, loadOnScrolledDown: false)
    }
    
    //MARKL: - TableView
    
    private func bindTable() {
        FavouriteManager.shared.favouriteBeers.bind(to: self.tvContent!.rx.items(cellIdentifier: BeerCell.nibName, cellType: BeerCell.self)) {
            index, model, cell in
            cell.beer = model
            }.disposed(by: self.disposeBag)
        
        self.tvContent?.rx.modelSelected(Beer.self).subscribe(onNext: { [weak self] (beer) in
            self?.navigationController?.pushViewController(BeerDetailsViewController(beer: .beerModel(beer)), animated: true)
        }).disposed(by: self.disposeBag)
    }

    private func registerCells() {
        self.tvContent?.register(UINib.init(nibName: BeerCell.nibName, bundle: nil), forCellReuseIdentifier: BeerCell.identifier)
    }
}

// MARK: - TabPresentable

extension FavouriteBeersViewController: TabPresentable {
    var tabPage: TabPages {
        return .favouriteBeers
    }
}
