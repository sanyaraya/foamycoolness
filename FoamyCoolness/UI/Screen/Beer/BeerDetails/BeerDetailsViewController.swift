//
//  BeerDetailsViewController.swift
//  FoamyCoolness
//
//  Created by Александр on 23.05.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit
import RxSwift
import RxDataSources
import RxCocoa
//MARK: - BeerDetailsModel

enum BeerDetailsModel {
    case beerId(String)
    case beerModel(Beer)
    
    func getId() -> String {
        switch self {
        case .beerId(let id):
            return id
        case .beerModel(let model):
            return model.id
        }
    }
}

//MARK: - BeerDetailsViewController

class BeerDetailsViewController: BaseViewController {

    // MARK: - Properties
    
    private var beer: BeerDetailsModel?

    private var model: BehaviorRelay<BeerDetailsModel>?
    
    private let imageLoader = ImageLoader()

    @IBOutlet weak var ivFavourite: FavouriteIndicator!
    @IBOutlet weak var ivBeer: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAbv: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var aiView: UIActivityIndicatorView!
    @IBOutlet weak var aiMain: UIActivityIndicatorView!

    let disposeBag = DisposeBag()
    
    // MARK: - Lifecycle
    
    convenience init(beer: BeerDetailsModel) {
        self.init(storyboardName: BeerDetailsViewController.storyboardName)
        self.beer = beer
        self.model = BehaviorRelay(value: beer)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.checkModels()
        
        FavouriteManager.shared.favouriteBeers.subscribe(onNext: { (beers) in
            self.checkIfBeerFavourite()
        }).disposed(by: self.disposeBag)
        
        self.ivFavourite.favouriteToggled.subscribe(onNext: { [unowned self](fav) in
            guard case .beerModel(let beerModel)? = self.beer else {
                return
            }
            if fav {
                FavouriteManager.shared.addBeer(beer: beerModel)
            } else {
                FavouriteManager.shared.removeBeer(beer: beerModel)
            }

        }).disposed(by: self.disposeBag)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        self.checkIfBeerFavourite()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }

    // MARK: - Model Preparing

    private func checkIfBeerFavourite() {
        guard let beer = self.beer else {
            return
        }
        self.ivFavourite.isFavourite.accept(FavouriteManager.shared.isFavourite(beerId: beer.getId()))
    }

    private func checkModels() {
        guard let beerModel = self.beer else {
            return
        }
        
        switch beerModel {
        case .beerId(let id):
            self.ivFavourite.isHidden = true
            self.loadModel(id: id)
        case .beerModel(let beer):
            self.configureView(detailedBeer: beer)
        }
    }

    private func loadModel(id: String) {
        self.aiMain.startAnimating()
        DataManager.shared.getBeerById(beerId: id).observeOn(MainScheduler.instance).subscribe(onNext: { [weak self](beer) in
            self?.beer = BeerDetailsModel.beerModel(beer)
            self?.configureView(detailedBeer: beer)
            self?.ivFavourite.isHidden = false
            self?.aiMain.stopAnimating()
        }).disposed(by: self.disposeBag)
    }

    // MARK: - UI

    private func configureView(detailedBeer: Beer) {
        self.title = detailedBeer.title
        self.lblTitle.text = detailedBeer.title

        self.checkIfBeerFavourite()
        self.lblDescription.text = detailedBeer.description

        if let abv = detailedBeer.abv {
            self.lblAbv.text = "ABV: \(abv.level)"
            self.paintABVLabel()
        } else {
            self.lblAbv.isHidden = true
        }

        self.aiView.startAnimating()
        
        self.imageLoader.loadMediumImage(model: detailedBeer)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: {[weak self](img) in
                self?.ivBeer.image = img ?? UIImage(named: ImagesNames.defaultBeer)
                self?.aiView.stopAnimating()
            }).disposed(by: self.disposeBag)
    }

    private func paintABVLabel() {
        
        guard let beerValue = self.beer, case .beerModel(let model) = beerValue else {
            return
        }
        
        guard let lblAbv = model.abv else {
            return
        }
        
        switch lblAbv.abvLevel {
        case .low:
            self.lblAbv.textColor = Colors.labelGreen
        case .middle:
            self.lblAbv.textColor = .orange
        default:
            self.lblAbv.textColor = .red
        }
    }
}

// MARK: - StoryboardInstantiable

extension BeerDetailsViewController: StoryboardInstantiable {
    static var storyboardName: String {
        return Storyboards.BeerDetails
    }
}
