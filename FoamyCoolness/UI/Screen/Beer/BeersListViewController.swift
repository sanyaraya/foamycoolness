//
//  BeersListViewController.swift
//  FoamyCoolness
//
//  Created by Александр on 23.05.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class BeersListViewController: BaseListViewController {

    // MARK: - Constants

    private enum Defaults {
        static let requestDelay = 400
    }

    // MARK: - Properties

    @IBOutlet weak var sbSearch: UISearchBar!

    private var pendingRequestWorkItem: DispatchWorkItem?

    let data = DataHolder<Beer>()

    let disposeBag = DisposeBag()
    
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        self.registerCells()

        self.tableManager = DataHolderTableManager(table: self.tvContent!)

        self.loadData()
        self.bindSearch()
        self.bindTable()
    }
    
    //MARK: - Binding
    
    private func bindTable() {
        self.tvContent?.rx.modelSelected(Beer.self).subscribe(onNext: { [unowned self](beer) in
            self.navigationController?.pushViewController(BeerDetailsViewController(beer: .beerModel(beer)), animated: true)
        }).disposed(by: self.disposeBag)
        
        let additionalLoad = self.tableManager!.loadMoreItems.flatMap { [unowned self](_)  -> Observable<Bool> in
            let search = self.sbSearch.text ?? ""
            return self.data.loadMore(search: search)
            }
        
        additionalLoad.subscribe().disposed(by: self.disposeBag)
        
        self.data.items.bind(to: self.tvContent!.rx.items(cellIdentifier: BeerCell.identifier, cellType: BeerCell.self)) {
            index, model, cell in
            cell.beer = model
            }.disposed(by: self.disposeBag)
    }
    
    private func bindSearch() {
        let searchInput = self.sbSearch.rx.text.orEmpty
            .throttle(.milliseconds(Defaults.requestDelay), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
        
        let search = searchInput.flatMap { (query)  -> Observable<Bool> in
            UserDefaults.standard.set(query, forKey: UserDefaultsKeys.lastBeerSearch)
            return self.data.reload(search: query)
            }.do(onError: { [unowned self](error) in
                self.showErrorAlert(error: error)
            } )
        
        let running = Observable.from([searchInput.map {_ in true},
                                       search.map {_ in false}.asObservable()]).merge().startWith(true).asDriver(onErrorJustReturn: false)
        
        running.asDriver().drive(self.spinner!.rx.isAnimating).disposed(by: self.disposeBag)

    }

    // MARK: - UserDefaults

    private func loadData() {
        let search = UserDefaults.standard.string(forKey: UserDefaultsKeys.lastBeerSearch) ?? ""
        self.sbSearch.text = search
    }
    
    // MARK: - Helpers

    private func registerCells() {
        self.tvContent?.register(UINib.init(nibName: BeerCell.nibName, bundle: nil), forCellReuseIdentifier: BeerCell.identifier)
    }

    private func checkData() {
        self.tvContent?.noItemsViewIsHidden = (self.data.itemsNumber != 0)
    }
}


// MARK: - UISearchBarDelegate

extension BeersListViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

// MARK: - TabPresentable

extension BeersListViewController: TabPresentable {
    var tabPage: TabPages {
        return .beersSearch
    }
}
