//
//  BeerCell.swift
//  FoamyCoolness
//
//  Created by Александр on 23.05.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit
import RxSwift

class BeerCell: UITableViewCell {

    // MARK: - Properties

    @IBOutlet weak var lblBeerTitle: UILabel!
    @IBOutlet weak var ivBeer: UIImageView!
    @IBOutlet weak var aiView: UIActivityIndicatorView!

    private let loader = ImageLoader()
    
    var disposeBag = DisposeBag()

    var beer: Beer? {
        didSet {
            guard let newBeer = beer else {
                return
            }
            self.lblBeerTitle.text = newBeer.title
            self.loadIcon()
        }
    }

    // MARK: - Lifecycle

    override func prepareForReuse() {
        super.prepareForReuse()
        self.disposeBag = DisposeBag()
    }

    private func loadIcon() {
        guard let model = beer else {
            return
        }
        
        self.aiView.startAnimating()
        self.loader.loadIcon(model: model).observeOn(MainScheduler.instance).subscribe(onNext: { (img) in
            self.ivBeer.image = img ?? UIImage(named: ImagesNames.defaultBeer)
            self.aiView.stopAnimating()
        }).disposed(by: self.disposeBag)
    }
}

// MARK: - AutoIndentifierCell

extension BeerCell: AutoIndentifierCell {}
