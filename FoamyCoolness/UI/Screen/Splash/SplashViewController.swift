//
//  SplashViewController.swift
//  Flickr
//
//  Created by Александр on 23.04.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    // MARK: - Constants

    private enum Defaults {
        static let widthProportion: CGFloat = 1 / 7.0
        static let heightProportion: CGFloat = 1 / 3.0
        static let animationDuration = 1.0
        static let animationRepeat = 0.9
    }

    // MARK: - Properties

    @IBOutlet weak var bgSplash: UIImageView!

    private var bubblesCount = 0
    private var timer: Timer?

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.isHidden = true
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if self.bubblesCount == 0 {
            self.startAnimations()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.timer?.invalidate()
        self.timer = nil
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.navigationBar.isHidden = false
    }

    // MARK: - Animation

    func startAnimations() {
        self.timer = Timer.scheduledTimer(withTimeInterval: Defaults.animationRepeat, repeats: true, block: { (_) in
                self.createBubble()
        })
    }

    func createBubble() {
        let bubble = UIImageView(image: UIImage.init(named: "\(ImagesNames.bubble)\(self.bubblesCount % 2 + 1).png"))
        self.bubblesCount += 1

        bubble.frame = self.bgSplash.bounds
        bubble.contentMode = .scaleAspectFit

        self.view.addSubview(bubble)

        let height = self.view.frame.height * Defaults.heightProportion
        let width = self.view.frame.width * Defaults.widthProportion

        let animationHelper = AnimationsHelper()

        let zigZag = animationHelper.createZigZag(startPoint: bubble.center, height: height, width: width)
        let path = animationHelper.createAnimationPath(path: zigZag, duration: Defaults.animationDuration)

        bubble.layer.add(path, forKey: nil)

        CATransaction.begin()

        CATransaction.setCompletionBlock {
            UIView.transition(with: bubble, duration: Defaults.animationDuration, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
                bubble.alpha = 0.0
            }, completion: { (_) in
                bubble.removeFromSuperview()
            })
        }
    }
}

// MARK: - StoryboardInstantiable

extension SplashViewController: StoryboardInstantiable {
    static var storyboardName: String {
        return Storyboards.Splash
    }
}
