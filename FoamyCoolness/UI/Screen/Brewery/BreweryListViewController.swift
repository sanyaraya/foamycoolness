//
//  BreweryListViewController.swift
//  FoamyCoolness
//
//  Created by Александр on 23.05.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit
import CoreLocation
import RxSwift
import RxCocoa

class BreweryListViewController: BaseListViewController {

    // MARK: - Properties

    let data = DataHolder<Brewery>()
    let locationManager = LocationManager()
    let disposeBag = DisposeBag()

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCells()
        self.bindTable()
        self.bindLocations()
        
        self.tableManager = DataHolderTableManager(table: self.tvContent!)
        self.locationManager.searchLocation()
    }
    
    //MARK: - binding
    
    private func bindTable() {
        self.tvContent?.rx.modelSelected(Brewery.self).subscribe(onNext: { (brewery) in
            self.navigationController?.pushViewController(BreweryDetailsViewController(brewery: brewery), animated: true)
        }).disposed(by: self.disposeBag)
        
        self.tableManager?.loadMoreItems.subscribe(onNext: { (_) in
            guard let coord = self.locationManager.location else {
                return
            }
            self.data.loadMore(coord: coord).subscribe(onNext: { (success) in
                
            }).disposed(by: self.disposeBag)
        }).disposed(by: self.disposeBag)
        
        self.data.items.bind(to: self.tvContent!.rx.items(cellIdentifier: BreweryCell.identifier, cellType: BreweryCell.self)) {
            index, model, cell in
            cell.brewery = model.brewery
            }.disposed(by: self.disposeBag)
    }
    
    private func bindLocations() {
        let locationsUpdating = self.locationManager.rx.didUpdateLocations
        
        let search = locationsUpdating.flatMap { (location) in
            return self.data.reload(coord: (location.coordinate.latitude, location.coordinate.longitude))
        }
        
        let locationsFailed = self.locationManager.rx.didGetErrorToDisplay
    
        let running = Observable.from([locationsUpdating.map{_ in return true},
                                       locationsFailed.map{_ in return false},
                                       search.map{_ in return false}.asObservable()])
            .merge()
            .asDriver(onErrorJustReturn: false)
        
        running.asDriver().drive(self.spinner!.rx.isAnimating).disposed(by: self.disposeBag)
       
        
        locationsFailed.subscribe(onNext: { [weak self](error) in
            self?.showErrorAlert(error: error)
        }).disposed(by: self.disposeBag)
    }

    private func registerCells() {
        self.tvContent?.register(UINib.init(nibName: BreweryCell.nibName, bundle: nil), forCellReuseIdentifier: BreweryCell.identifier)
    }

    // MARK: - helpers

    private func checkData() {
        self.tvContent?.noItemsViewIsHidden = (self.data.itemsNumber != 0)
    }
}

// MARK: - TabPresentable

extension BreweryListViewController: TabPresentable {
    var tabPage: TabPages {
        return .breweriesNearby
    }
}

//38.5985037 -90.2093428
//39.7236683 -105.0006015
