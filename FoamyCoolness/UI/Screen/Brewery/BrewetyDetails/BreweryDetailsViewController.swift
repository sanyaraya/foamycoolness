//
//  BreweryDetails.swift
//  FoamyCoolness
//
//  Created by Александр on 24.05.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class BreweryDetailsViewController: BaseViewController {
    // MARK: - Constants

    private enum TableSections: Int {
        case header
        case beers
        case count
    }
    enum BrewerySectionType {
        case header(Brewery?)
        case beer(Beer?)
    }
    // MARK: - Properties

    @IBOutlet weak var aiView: UIActivityIndicatorView!
    @IBOutlet weak var tvContent: UITableView!

    var brewery: Brewery?

    let disposedBag = DisposeBag()

    let sections = BehaviorRelay<[BrewerySectionType]>(value: [])
    
    // MARK: - Lifecycle

    convenience init(brewery: Brewery) {
        self.init(storyboardName: BreweryDetailsViewController.storyboardName)
        self.brewery = brewery
        self.sections.accept([.header(brewery)])
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCells()
        self.bindTable()
        self.loadBeers()
        
        if let name =  self.brewery?.brewery.name {
            self.title = name
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK: - Binding
    
    private func bindTable() {
        
        self.tvContent.rx.modelSelected(BrewerySectionType.self).subscribe(onNext: { (beer) in
            switch beer {
            case .beer(let model):
                if let beerModel = model {
                    let beerDetails = BeerDetailsModel.beerId(beerModel.id)
                    self.navigationController?.pushViewController(BeerDetailsViewController(beer: beerDetails), animated: true)
                }
            default:
                break
            }
            
        }).disposed(by: self.disposedBag)
        
        self.sections.bind(to: self.tvContent.rx.items) {
            table, index, element in
            let indexPath = IndexPath(row: index, section: 0)
            
            switch element {
            case .beer(let beerModel):
                let cell = table.dequeueReusableCell(withIdentifier: BeerCell.identifier, for: indexPath) as! BeerCell
                cell.beer = beerModel
                return cell
            case .header(let headerModel):
                let header = table.dequeueReusableCell(withIdentifier: BreweyDetailsCell.identifier, for: indexPath) as! BreweyDetailsCell
                header.brewery = headerModel
                return header
                
            }
            }.disposed(by: self.disposedBag)
    }

    // MARK: - helpers

    private func registerCells() {
        self.tvContent?.register(UINib.init(nibName: BeerCell.nibName, bundle: nil), forCellReuseIdentifier: BeerCell.identifier)

        self.tvContent?.register(UINib.init(nibName: BreweyDetailsCell.nibName, bundle: nil), forCellReuseIdentifier: BreweyDetailsCell.identifier)
    }

    private func loadBeers() {
        self.aiView.startAnimating()

        guard let breweryId = self.brewery?.brewery.id else {
            return
        }
        
        let search = DataManager.shared.getBeersByBreweryId(brewId: breweryId)
            .observeOn(MainScheduler.instance)
        
        search.subscribe(onNext: { [weak self](beers) in
            guard let this = self else {
                return
            }
            this.aiView.stopAnimating()
            let newBeers = beers.map{value in BrewerySectionType.beer(value)}
            let newValue = this.sections.value + newBeers
            this.sections.accept(newValue)
        }).disposed(by: self.disposedBag)

    }
}

// MARK: - StoryboardInstantiable

extension BreweryDetailsViewController: StoryboardInstantiable {
    static var storyboardName: String {
        return Storyboards.BreweryDetails
    }
}

