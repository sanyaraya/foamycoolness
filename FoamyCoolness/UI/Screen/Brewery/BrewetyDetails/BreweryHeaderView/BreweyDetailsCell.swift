//
//  BreweyDetailsCell.swift
//  FoamyCoolness
//
//  Created by Александр on 24.05.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class BreweyDetailsCell: UITableViewCell {

    // MARK: - Constants

    private enum SocialButtons: Int {
        case web
        case phone
    }

    // MARK: - Properties

    @IBOutlet weak var aiView: UIActivityIndicatorView!
    @IBOutlet weak var ivBrewery: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblDistance: UILabel!

    @IBOutlet weak var btnWeb: UIButton!
    @IBOutlet weak var btnCall: UIButton!

    private let imageLoader = ImageLoader()
    private let disposeBage = DisposeBag()
    
    // MARK: - Getters
    
    var brewery: Brewery? {
        didSet {
            guard let brew = brewery else {
                return
            }

            self.lblTitle.text = "\(brew.brewery.name)"

            self.lblDescription.text = brew.brewery.description

            self.btnWeb.isHidden = (brew.brewery.website == nil)
            self.btnCall.isHidden = (brew.phone == nil)

            if let address = brew.streetAddress {
                self.lblAddress.isHidden = false
                self.lblAddress.text = "Address: \(address)"
            } else {
                self.lblAddress.isHidden = true
            }

            if let distance = brew.distance?.distance {
                self.lblDistance.isHidden = false
                self.lblDistance.text = "Distance: \(distance) miles"
            } else {
                self.lblDistance.isHidden = true
            }

            self.paintDistanceLabel()

            self.loadImage()
        }
    }

    private func loadImage() {
        guard let url = self.brewery?.brewery.imageURL else {
            return
        }
        //self.aiView.startAnimating()
        
        self.imageLoader.loadImageUrl(url: url).observeOn(MainScheduler.instance)
            .subscribe(onNext: {[weak self](img) in
                self?.ivBrewery.image = img ?? UIImage(named: ImagesNames.defaultBrewery)

        }).disposed(by: self.disposeBage)
    }

    // MARK: - UI

    private func paintDistanceLabel() {
        guard let distance = self.brewery?.distance else {
            return
        }

        switch distance.distanceEstimate {
            case .nearby:
                self.lblDistance.textColor = Colors.labelGreen
            case .normal:
                self.lblDistance.textColor = .orange
            default:
                self.lblDistance.textColor = .red
        }
    }
    
    //MARK: - lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.bindButtons()
    }
    
    //MARK: - binding

    private func bindButtons() {
        self.btnWeb.rx.tap.subscribe(onNext: { (_) in
            visitWeb(webSite: self.brewery?.brewery.website)
        }).disposed(by: self.disposeBage)
        
        self.btnCall.rx.tap.subscribe(onNext: { (_) in
            makePhoneCall(phone: self.brewery?.phone)
        }).disposed(by: self.disposeBage)
    }
}

// MARK: - AutoIndentifierCell

extension BreweyDetailsCell: AutoIndentifierCell {}
