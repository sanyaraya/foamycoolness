//
//  BreweryCell.swift
//  FoamyCoolness
//
//  Created by Александр on 23.05.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit

class BreweryCell: UITableViewCell {

    // MARK: - Properties

    @IBOutlet weak var lblBrewery: UILabel!

    var brewery: BreweryDetails? {
        didSet {
            guard let model = brewery else {
                return
            }
            self.lblBrewery.text = "\(model.name)"
        }
    }
}

// MARK: - AutoIndentifierCell

extension BreweryCell: AutoIndentifierCell {}
