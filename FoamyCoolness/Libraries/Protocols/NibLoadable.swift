//
//  NibLoadable.swift
//  Together
//
//  Created by Александр on 04.09.2018.
//  Copyright © 2018 com.Think.Dev.Together. All rights reserved.
//

import UIKit

protocol NibLoadable {

    static var nibName: String? { get }

    func loadNib()

}

extension NibLoadable where Self: UIView {

    func loadNib() {
        guard let name = Self.nibName else {
            Bundle.main.loadNibNamed(String(describing: self.classForCoder), owner: self, options: nil)
            return
        }
        Bundle.main.loadNibNamed(name, owner: self, options: nil)
    }

}
