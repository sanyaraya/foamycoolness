//
//  TabPresentable.swift
//  FoamyCoolness
//
//  Created by Александр on 29.05.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit

protocol TabPresentable {
    var tabPage: TabPages { get }
}
