//
//  BaseViewController.m
//  FoamyCoolness
//
//  Created by Александр on 31.05.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (instancetype)initWithStoryboardName:(NSString *)name {
    return [[UIStoryboard storyboardWithName:name bundle:nil] instantiateViewControllerWithIdentifier: NSStringFromClass([self class]).pathExtension];
}


@end
