//
//  SocialHelper.swift
//  FoamyCoolness
//
//  Created by Александр on 25.05.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import UIKit

// MARK: - Phone

func makePhoneCall(phone: String?) {
    guard let number = phone, let url = URL(string: "tel://\(number)") else {
        return
    }
    UIApplication.shared.open(url, options: [:], completionHandler: nil)
}

// MARK: - Web

func visitWeb(webSite: String?) {
    guard let web = webSite, let url = URL(string: web) else {
        return
    }
    UIApplication.shared.open(url, options: [:], completionHandler: nil)
}
