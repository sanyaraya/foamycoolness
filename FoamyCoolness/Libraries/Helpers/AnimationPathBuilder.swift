//
//  AnimationPathBuilder.swift
//  FoamyCoolness
//
//  Created by Александр on 28.05.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import Foundation
import UIKit

class AnimationsHelper: NSObject {

    // MARK: - Constants

    private enum Defaults {
        static let partsNumber: CGFloat = 3.0
        static let animatePosition = "position"
    }

    // MARK: - AnimationsMethods

    func createZigZag(startPoint: CGPoint, height: CGFloat, width: CGFloat) -> UIBezierPath {
        let zigzagPath = UIBezierPath()
        let endX = startPoint.x
        let endY = startPoint.y - height

        let cp1 = CGPoint(x: startPoint.x - width, y: startPoint.y - height / Defaults.partsNumber)
        let cp2 = CGPoint(x: startPoint.x + width, y: startPoint.y - 2 * height / Defaults.partsNumber)

        zigzagPath.move(to: startPoint)
        zigzagPath.addCurve(to: CGPoint(x: endX, y: endY), controlPoint1: cp1, controlPoint2: cp2)

        return zigzagPath
    }

    func createAnimationPath(path: UIBezierPath, duration: Double) -> CAKeyframeAnimation {
        let pathAnimation = CAKeyframeAnimation(keyPath: Defaults.animatePosition)
        pathAnimation.duration = duration
        pathAnimation.path = path.cgPath
        pathAnimation.fillMode = CAMediaTimingFillMode.forwards
        pathAnimation.isRemovedOnCompletion = false
        return pathAnimation
    }

}
