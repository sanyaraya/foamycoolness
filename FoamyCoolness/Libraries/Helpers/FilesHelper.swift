//
//  FilesHelper.swift
//  FoamyCoolness
//
//  Created by Александр on 27.05.2019.
//  Copyright © 2019 Александр. All rights reserved.
//

import Foundation

func documentsDirectory() -> URL {
    let paths = FileManager.default.urls(for: .documentDirectory,
                                         in: .userDomainMask)
    return paths[0]
}
